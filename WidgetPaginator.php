<?php
  namespace Ub\Admin;

  /**
   * Class WidgetPaginator
   * @package Ub\Admin
   */
  class WidgetPaginator extends \Uc\Widget {

    public function getViewFile() {
      return 'widgetPaginator';
    }
  }